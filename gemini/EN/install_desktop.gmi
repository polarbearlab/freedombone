=> index.gmi ⯇ Go Back

# Installing Freedombone on Desktop or Laptop

``` laptop logo
         ._________________.
         |.---------------.|
         || >_            ||
         ||               ||
         ||               ||
─────────||               ||──────────
         ||               ||
         ||_______________||
         /.-.-.-.-.-.-.-.-.\
        /.-.-.-.-.-.-.-.-.-.\
       /.-.-.-.-.-.-.-.-.-.-.\
      /        /      \       \ 
     /________/________\_______\ 
     \_________________________/
```

Do this on a separate machine, not the laptop which will become your Freedombone server

## ⃟ Step 1

Download freedombone-main-all-amd64.img.xz and extract it.

    wget -c https://freedombone.net/downloads/freedombone-main-all-amd64.img.xz

If the direct download desn't work then there are some Magnet links and you can use a bittorrent client to obtain them.

=> https://freedombone.net/downloads/magnet_links.txt Magnet links

You can use the unxz command to extract the image file.

You can also download the signature and check it using this public key (CC2536191FA7C33F)

``` lock and key logo
──▄▀▀▀▄───────────────
──█───█───────────────
─███████─────────▄▀▀▄─
░██─▀─██░░█▀█▀▀▀▀█░░█░
░███▄███░░▀░▀░░░░░▀▀░░
```

-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEWZBueBYJKwYBBAHaRw8BAQdAKx1t6wL0RTuU6/IBjngMbVJJ3Wg/3UW73/PV
I47xKTS0IUJvYiBNb3R0cmFtIDxib2JAZnJlZWRvbWJvbmUubmV0PoiQBBMWCAA4
FiEEmruCwAq/OfgmgEh9zCU2GR+nwz8FAlmQbngCGwMFCwkIBwMFFQoJCAsFFgID
AQACHgECF4AACgkQzCU2GR+nwz/9sAD/YgsHnVszHNz1zlVc5EgY1ByDupiJpHj0
XsLYk3AbNRgBALn45RqgD4eWHpmOriH09H5Rc5V9iN4+OiGUn2AzJ6oHuDgEWZBu
eBIKKwYBBAGXVQEFAQEHQPRBG2ZQJce475S3e0Dxeb0Fz5WdEu2q3GYLo4QG+4Ry
AwEIB4h4BBgWCAAgFiEEmruCwAq/OfgmgEh9zCU2GR+nwz8FAlmQbngCGwwACgkQ
zCU2GR+nwz+OswD+JOoyBku9FzuWoVoOevU2HH+bPOMDgY2OLnST9ZSyHkMBAMcK
fnaZ2Wi050483Sj2RmQRpb99Dod7rVZTDtCqXk0J
=gv5G
-----END PGP PUBLIC KEY BLOCK-----

    wget https://freedombone.net/downloads/freedombone_public_key.txt
    gpg --import freedombone_public_key.txt
    wget https://freedombone.net/downloads/freedombone-main-all-amd64.img.xz.asc
    gpg --verify freedombone-main-all-amd64.img.xz.asc

Or there is a version which only uses onion addresses, which means that you don't need to buy a domain name or set up dynamic DNS.

    wget -c https://freedombone.net/downloads/freedombone-onion-all-amd64.img.xz
    wget https://freedombone.net/downloads/freedombone_public_key.txt
    gpg --import freedombone_public_key.txt
    wget https://freedombone.net/downloads/freedombone-onion-all-amd64.img.xz.asc
    gpg --verify freedombone-onion-all-amd64.img.xz.asc

## ⃟ Step 2

Plug in a USB drive or connect an SSD which you'll use in the server, then find its device name, which usually begins with /dev/sd. One way of doing this is with the command

    ls /dev/sd*

Before and after attaching the USB or SSD drive

## ⃟ Step 3

Now copy the extracted image to the USB drive or SSD. This may take quite a while

    sudo dd bs=1M if=freedombone-main-all-amd64.img of=/dev/sdX conv=fdatasync,sync,noerror

## ⃟ Step 4

You may want to make sure that you use the whole of the available disk space on the USB drive or SSD, using a tool such as gparted to resize the partition

## ⃟ Step 5

Remove the USB drive or SSD and plug it into the laptop which will be your server. If it's a USB drive then plug it directly into the laptop, not through a USB hub. If the laptop has EFI then make sure it's deactivated in the BIOS.

## ⃟ Step 6

Plug your server laptop into one of the ethernet sockets on your internet router using a USB patch cable (cat5 or cat6). Also make sure that the server laptop has a mains power supply

## ⃟ Step 7

Power on the laptop and boot from the USB drive or SSD. You may need to change the BIOS settings so that it boots from this drive by default

## ⃟ Step 8

Ensure that mDNS/zeroconf is enabled on your internet router. The router settings are often accessed via 192.168.2.1 or 192.168.10.1 or 192.168.1.254

## ⃟ Step 9

If avahi is not available on your laptop (not the Freedombone server) then make sure you install it

On Debian

    apt install avahi-utils avahi-dnsconfd

On Arch/Parabola

    sudo pacman -S avahi nss-mdns
    sudo sed -i 's|hosts:.*|hosts: files mdns_minimal [NOTFOUND=return] dns myhostname|g' /etc/nsswitch.conf

## ⃟ Step 10

Browse the local network with

    avahi-browse -at

If eveything is working you should notice that a system called freedombone appears in the list, with a http service

## ⃟ And finally, step 11

Open a web browser which is not a Tor browser and navigate to http://freedombone/admin, or if that doesn't work then try http://freedombone.local/admin. If your new server isn't named "freedombone" on your local network then log into your internet router and see what local name or local IP address it has been assigned. If all else fails then navigate to [server local IP]/admin.

Congratulations! You are now ready to begin setting up the server and installing apps. You will need (if you don't use the onion-only version) to have purchased a domain name, and have a dynamic DNS account or equivalent arrangement so that the server domain name is resolvable from the wider internet. If you use the version which only uses onion addresses, you don't need to buy a domain name or set up dynamic DNS as your freedombone sever will be reachable through the Tor network and having assigned an [address].onion url.