#+TITLE: Freedombone
#+AUTHOR: Bob Mottram
#+EMAIL: bob@freedombone.net
#+KEYWORDS: freedombone, apps
#+DESCRIPTION: List of apps available on freedombone
#+OPTIONS: ^:nil toc:nil num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="freedombone.css" />

#+attr_html: :width 80% :height 10% :align center
[[file:images/logo.png]]

This system comes in three versions:

 * Standard server version: for the ordinary internet using conventional domain names
 * /Onion only/ server version: uses onion addresses. Doesn't need a domain name or port forwarding
 * [[./mesh.html][Mesh peer/client version]]: off-grid autonomous operation, separate from the internet. No servers.
* Server apps

These apps are available on all server versions of the system, either via clearnet or onion addresses.

#+BEGIN_CENTER
#+ATTR_HTML: :border -1
| AberMUSH - MUD game played over telnet                                               |
| [[./help_akaunting.html][Akaunting]] - A web based accounts system for small businesses or freelancers.         |
| [[./help_babybuddy.html][BabyBuddy]] - Helps caregivers track sleep, feedings, diaper changes, and tummy time.  |
| [[./help_bludit.html][Bludit]] - Databaseless blogging system.                                               |
| Chess - The classic chess server                                                     |
| Crossfire - Gauntlet-like multi-player RPG                                           |
| [[./help_cryptpad.html][CryptPad]] - Collaborative documents, presentations, votes and drawing.                |
| [[./help_datserver.html][Datserver]] - Seed dat protocol files from your server to make them always accessible. |
| [[./help_dlna.html][DLNA]] - Play media on UPNP/DLNA supported devices                                     |
| [[./help_dokuwiki.html][Dokuwiki]] - A databaseless wiki system.                                               |
| [[./help_etesync.html][EteSync]] - End-to-end encrypted sync of calendar and contacts between devices.        |
| [[./help_etherpad.html][Etherpad]] - Collaborative document creation.                                          |
| [[./help_fedwiki.html][Federated wiki]] - A new approach to creating wiki content.                            |
| Flightgear - Multi-player server for the flight simulator                            |
| Freeciv - Multi-player server for the city building game                             |
| Gemini - Gemini protocol server, similar to gopher but with encryption               |
| [[./help_gogs.html][Gogs]] - Lightweight git project hosting system.                                       |
| Grocy - Inventory management for your home                                           |
| [[./help_htmly.html][HTMLy]] - Databaseless blogging system.                                                |
| [[./help_irc.html][IRC Server]] - Chat server with bouncer.                                               |
| [[./help_icecast.html][Icecast media stream]] - Make your own internet radio station.                         |
| [[./help_kanboard.html][KanBoard]] - Simple kanban system for managing projects or TODO lists.                 |
| [[./help_lychee.html][Lychee]] - Make your photo albums available on the web.                                |
| Matrix - The Synapse matrix chat server                                              |
| Minetest - Multi-player server for Minetest                                          |
| MPD - Stream music from your server                                                  |
| [[./help_mumble.html][Mumble]] - VoIP and text chat system.                                                  |
| [[./help_nextcloud.html][NextCloud]] - File storage, chat, webmail and video conferencing.                      |
| [[./help_pihole.html][Pi-Hole]] - Block web ads at the DNS level.                                            |
| Poker - Multi-player server for PokerTH                                              |
| [[./help_privatebin.html][PrivateBin]] - Pastebin where the server has zero knowledge of the content.            |
| Riot - RiotWeb client for Matrix                                                     |
| [[./help_rocketchat.html][Rocketchat]] - Non-federated chat server (x86 systems only).                           |
| Rsync - The classic file syncronization tool                                         |
| [[./help_smolrss.html][Smol RSS]] - Minimal RSS reader.                                                       |
| [[./help_syncthing.html][Syncthing]] - Synchronise files across all of your devices.                            |
| [[./help_turtl.html][Turtl]] - Privately create and share notes and images.                                 |
| [[./help_xmpp.html][XMPP]] - Chat server.                                                                  |
#+END_CENTER

* Standard server apps

These apps are not available on the /onion only/ versions, since they can't currently be onion routed.

#+BEGIN_CENTER
#+ATTR_HTML: :border -1
| [[./help_friendica.html][Friendica]] - Federated social network system.                                         |
| [[./help_hubzilla.html][Hubzilla]] - Web publishing platform with social network like features.                |
| [[./help_zap.html][Zap]] - Nomadic social network server                                                  |
#+END_CENTER

* Mesh apps

#+BEGIN_CENTER
#+ATTR_HTML: :border -1
| [[./help_cryptpad.html][CryptPad]] - Collaborate on documents, presentations, votes and drawing while off the grid |
| IPFS public folder - Share files/audio/video on the mesh.                                |
| Kdenlive - Edit videos                                                                   |
| Pelican - Static blogging with a large choice of themes                                  |
| qTox - The main text, VoIP and video chat application                                    |
| Shotwell - Manage and edit photos                                                        |
#+END_CENTER
